(function ($) {
  "use strict";

  let siteHeader      = $('.site-header'),
      shMobileMenu    = siteHeader.find('.site-header__mobile-menu'),
      openMobileMenu  = siteHeader.find('.open-mobile-menu'),
      closeMobileMenu = siteHeader.find('.close-mobile-menu');

  openMobileMenu.on('click', function () {
    shMobileMenu.addClass('site-header__mobile-menu_active');
    $('body').prepend('<div class="hng-overlay"></div>');
  });

  closeMobileMenu.on('click', function () {
    shMobileMenu.removeClass('site-header__mobile-menu_active');
    $('.hng-overlay').remove();
  });

})(jQuery);