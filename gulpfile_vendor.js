module.exports = function (gulp, plugins) {
  let vendorDir = './node_modules/',
    vendorArr = {
      jquery: {
        JS: 'jquery/dist/jquery.min.js'
      }
    },
    arr = [],
    i = 0;

  for (let prop in vendorArr) {
    arr[i] = '' + prop;
    i++;
  }

  // Vendor CSS
  let css = arr.map(function (arr) {
    let arrCSS = [];
    if(typeof vendorArr[arr].CSS === 'object') {
      vendorArr[arr].CSS.map(function (item, i) {
        arrCSS[i] = vendorDir + item;
      })
    } else {
      arrCSS[0] = vendorDir + vendorArr[arr].CSS;
    }

    return gulp.src(arrCSS ? arrCSS : [])
      .pipe(gulp.dest('dist/vendor/' + arr + '/css'));
  });

  // Vendor JS
  let js = arr.map(function (arr) {
    let arrJS = [];
    if(typeof vendorArr[arr].JS === 'object') {
      vendorArr[arr].JS.map(function (item, i) {
        arrJS[i] = vendorDir + item;
      })
    } else {
      arrJS[0] = vendorDir + vendorArr[arr].JS;
    }

    let min = arr === 'lazysizes';

    return gulp.src(arrJS ? arrJS : [])
      .pipe(plugins.if(min, plugins.concat(arr + '.min.js')))
      .pipe(plugins.if(min, plugins.uglify()))
      .pipe(gulp.dest('dist/vendor/' + arr + '/js'));
  });

  return function () {
    return plugins.merge(css, js);
  };
};