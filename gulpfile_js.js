module.exports = function (gulp, plugins, getFolders) {
  function onError(e) {
    console.log(e.toString());
    this.emit('end');
  }

  /* Paths */
  let src = {
      main: 'src/js/**/[^_]*.js',
      components: 'src/components'
    },
    dist = {
      main: 'dist/js',
      mainUncompressed: 'dist/js/uncompressed',
      components: 'dist/js/components',
      componentsUncompressed: 'dist/js/components/uncompressed'
    };
  /* End Paths */

  return function () {

    gulp.src(src.main)
      .pipe(plugins.plumber({errorHandler: onError}))
      .pipe(plugins.changedInPlace({firstPass: true}))
      .pipe(gulp.dest(dist.mainUncompressed))
      .pipe(plugins.uglifyES())
      //.pipe(plugins.debug({title: 'JS:'}))
      .pipe(gulp.dest(dist.main));

    let components = getFolders(src.components).map(function (folder) {
      return gulp.src(plugins.path.join(src.components, folder, '/*.js'))
        .pipe(plugins.plumber({errorHandler: onError}))
        .pipe(plugins.concat(folder + '.js'))
        .pipe(plugins.changedInPlace({firstPass: true}))
        .pipe(gulp.dest(dist.componentsUncompressed))
        .pipe(plugins.uglifyES())
        //.pipe(plugins.debug({title: 'JS:'}))
        .pipe(gulp.dest(dist.components));
    });

    return plugins.merge(components);
  };
};